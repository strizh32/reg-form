import DatePicker from '../DatePicker/DatePicker.vue';
import types from '../../store/types';
import debounce from 'debounce';

const months = [
  'january',
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december'
];

export default {
  name: 'RegForm',
  data() {
    return {
      pickerIsVisible: false,
      countriesListVisible: false,
      citiesListVisible: false
    }
  },
  computed: {
    dateValue() {
      const bDate = this.$store.state.bDate;

      return `${months[bDate.month]}, ${bDate.day} ${bDate.year}`;
    }
  },
  components: {
    DatePicker
  },
  methods: {
    formSubmit() {
      alert(JSON.stringify(this.$store.state));
    },
    dateChange(e) {
      this.$store.dispatch(types.SET_BDATE, {bDate: e});
    },
    setCountry(country) {
      this.$store.dispatch(types.SET_COUNTRY, {country});

      this.countriesListVisible = false;
    },
    setCity(city) {
      this.$store.dispatch(types.SET_CITY, {city});

      this.citiesListVisible = false;
    },
    countryInput: debounce(function (e) {
      const country = e.target.value;
      this.countriesListVisible = true;
      this.$store.dispatch(types.SET_COUNTRY, {country})
      .then(() => this.$store.dispatch(types.SET_COUNTRIES))
    }, 300),
    cityInput: debounce(function (e) {
      const city = e.target.value;
      this.citiesListVisible = true;
      this.$store.dispatch(types.SET_CITY, {city})
      .then(() => this.$store.dispatch(types.SET_CITIES))
    }, 300),
    datePickerClick() {
      this.pickerIsVisible = true;
    },
    hideCountries() {
      this.countriesListVisible = false;
    },
    hideCities() {
      this.citiesListVisible = false;
    },
    hidePicker() {
      this.pickerIsVisible = false;
    },
    updateLogin(e) {
      this.$store.dispatch(types.SET_LOGIN, {login: e.target.value});
    },
    updateEmail(e) {
      this.$store.dispatch(types.SET_EMAIL, {email: e.target.value});
    },
    updatePassword(e) {
      this.$store.dispatch(types.SET_PASSWORD, {password: e.target.value});
    },
    updateFirstName(e) {
      this.$store.dispatch(types.SET_FNAME, {firstName: e.target.value});
    },
    updateLastName(e) {
      this.$store.dispatch(types.SET_LNAME, {lastName: e.target.value});
    },
    updateZipCode(e) {
      this.$store.dispatch(types.SET_ZIPCODE, {zipCode: e.target.value});
    }
  }
}