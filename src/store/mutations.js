import types from './types';

export default {
  [types.SET_ZIPCODE](state, payload) {
    state.zipCode = payload;
  },
  [types.SET_PASSWORD](state, payload) {
    state.password = payload;
  },
  [types.SET_LOGIN](state, payload) {
    state.login = payload;
  },
  [types.SET_LNAME](state, payload) {
    state.lastName = payload;
  },
  [types.SET_FNAME](state, payload) {
    state.firstName = payload;
  },
  [types.SET_EMAIL](state, payload) {
    state.email = payload;
  },
  [types.SET_BDATE](state, payload) {
    state.bDate = payload;
  },
  [types.SET_COUNTRY](state, payload) {
    state.country = payload;
  },
  [types.SET_CITY](state, payload) {
    state.city = payload;
  },
  [types.SET_COUNTRIES](state, payload) {
    state.countries = payload;
  },
  [types.SET_CITIES](state, payload) {
    state.cities = payload;
  }
}
