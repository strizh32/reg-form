export default {
  bDate: {
    day: 0,
    month: 0,
    year: 0
  },
  country: '',
  city: '',
  login: '',
  email: '',
  password: '',
  firstName: '',
  lastName: '',
  zipCode: 0,
  countries: [],
  cities: []
};