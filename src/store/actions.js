import types from './types';
import axios from 'axios';
import qs from 'qs';

export default {
  [types.SET_CITY](store, payload) {
    store.commit(types.SET_CITY, payload.city);
  },
  [types.SET_COUNTRY](store, payload) {
    store.commit(types.SET_COUNTRY, payload.country);
  },
  [types.SET_BDATE](store, payload) {
    store.commit(types.SET_BDATE, payload.bDate);
  },
  [types.SET_EMAIL](store, payload) {
    store.commit(types.SET_EMAIL, payload.email);
  },
  [types.SET_FNAME](store, payload) {
    store.commit(types.SET_FNAME, payload.firstName);
  },
  [types.SET_LNAME](store, payload) {
    store.commit(types.SET_LNAME, payload.lastName);
  },
  [types.SET_LOGIN](store, payload) {
    store.commit(types.SET_LOGIN, payload.login);
  },
  [types.SET_PASSWORD](store, payload) {
    store.commit(types.SET_PASSWORD, payload.password);
  },
  [types.SET_ZIPCODE](store, payload) {
    store.commit(types.SET_ZIPCODE, payload.zipCode);
  },
  [types.SET_COUNTRIES](store) {
    // simple nginx proxy
    const ip = '138.68.150.184';
    // const url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
    const url = `http://${ip}/maps/api/place/autocomplete/json?`;
    const params = {
      input: store.state.country,
      key: 'AIzaSyBRjTNeVsa8dlQmqIZDM-4mAHa-7TeAIP0',
      types: '(regions)'
    };
    axios.get(url + qs.stringify(params))
    .then(resp => {
      const predictions = resp.data.predictions;

      if (predictions.length < 1)
        store.commit(types.SET_COUNTRIES, []);
      else {
        const countries = predictions.map(country => country.description);
        store.commit(types.SET_COUNTRIES, countries);
      }

    })
    .catch(err => console.warn(err));
  },
  [types.SET_CITIES](store) {
    // simple nginx proxy
    const ip = '138.68.150.184';
    // const url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
    const url = `http://${ip}/maps/api/place/autocomplete/json?`;
    const params = {
      input: store.state.city,
      key: 'AIzaSyBRjTNeVsa8dlQmqIZDM-4mAHa-7TeAIP0',
      types: '(cities)'
    };
    axios.get(url + qs.stringify(params))
    .then(resp => {
      const predictions = resp.data.predictions;

      if (predictions.length < 1)
        store.commit(types.SET_CITIES, []);
      else {
        const cities = predictions.map(cities => cities.description);
        store.commit(types.SET_CITIES, cities);
      }

    })
    .catch(err => console.warn(err));
  }
}